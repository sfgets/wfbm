FROM photon:latest

ARG BM_USER="bm"  
ARG BM_DIR="/bm/"
ARG PKGS="shadow python3-pip wget gnupg tar gzip"

ENV TELEGRAF_VERSION 1.13.4

RUN tdnf -y upgrade && tdnf -y install ${PKGS} && \
    mkdir -p ${BM_DIR} /usr/src /etc/telegraf && \
    groupadd -r ${BM_USER} -g 1001 && useradd -u 1001 -r -g ${BM_USER} -m -d ${BM_DIR} -s /sbin/nologin ${BM_USER} && \
    chown -R ${BM_USER}: ${BM_DIR} && \
    for key in 05CE15085FC09D18E99EFB22684A14CF2582E0C5; do \
        gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key" || \
        gpg --keyserver pgp.mit.edu --recv-keys "$key" || \
        gpg --keyserver keyserver.pgp.com --recv-keys "$key" ; \
    done && \
    wget --no-verbose https://dl.influxdata.com/telegraf/releases/telegraf-${TELEGRAF_VERSION}-static_linux_amd64.tar.gz.asc && \
    wget --no-verbose https://dl.influxdata.com/telegraf/releases/telegraf-${TELEGRAF_VERSION}-static_linux_amd64.tar.gz && \
    gpg --batch --verify telegraf-${TELEGRAF_VERSION}-static_linux_amd64.tar.gz.asc telegraf-${TELEGRAF_VERSION}-static_linux_amd64.tar.gz && \
    tar -C /usr/src -xzf telegraf-${TELEGRAF_VERSION}-static_linux_amd64.tar.gz && \
    mv /usr/src/telegraf*/telegraf.conf /etc/telegraf/ && \
    chmod +x /usr/src/telegraf*/* && \
    cp -a /usr/src/telegraf*/* /usr/bin/ && \
    rm -rf *.tar.gz* /usr/src /root/.gnupg && \
    pip3 install -U pip requests && \
    tdnf -y erase ${PKGS} && tdnf clean all

ADD --chown=1001 runscope.py ${BM_DIR}

EXPOSE 8125/udp 8092/udp 8094

COPY entrypoint.sh /entrypoint.sh

USER ${BM_USER}

ENTRYPOINT ["/entrypoint.sh"]
CMD ["telegraf"]
