import os
import platform
import argparse
import json
import sys
import requests
import io
import tempfile
import time
from datetime import datetime, timedelta

runscope_service_url = "https://api.runscope.com/buckets"
cache_expiry_min = 60
cache_dir = '/tmp' if platform.system() == 'Darwin' else tempfile.gettempdir()
cache_file = os.path.join(cache_dir,'test_meta_info.txt')
cache_file_test_finished_at = os.path.join(cache_dir,'test_finished_at.txt')

try:
    to_unicode = unicode
except NameError:
    to_unicode = str


def cache_info(json_obj, file_location):
    try :
        with io.open(file_location, 'w', encoding='utf-8') as outfile:
           str_ = json.dumps(json_obj,
                      indent=4, sort_keys=True,
                      separators=(',', ': '), ensure_ascii=False)
           outfile.write(to_unicode(str_))
           outfile.close()
    except Exception as e:
        handle_error("error writing to cache: " + str(e))

def is_cache_stale():
    if os.path.exists(cache_file):
       file_mod_time = datetime.fromtimestamp(os.stat(cache_file).st_mtime)
       now = datetime.today()
       max_cache_time = timedelta(minutes=cache_expiry_min)
       if now-file_mod_time < max_cache_time:
          return False
    return True

def get_cached_test_info(file_location):
    try :
        with open(file_location) as test_mi:
            if test_mi is None:
                return None
            else:
                mi = json.load(test_mi)
                test_mi.close()
                return mi
    except ValueError as ve:
        return None
    except Exception as e:
        handle_error("error reading from cache: " + str(e))

def handle_error(error_message):
    sys.stderr.write("ERROR:|Runscope| " + error_message)
    sys.exit(1)

def get_runscope_api_response(url):
    try :
        headers = {}
        headers["Authorization"] = "Bearer {} ".format(args.token[0])
        headers["Content-type"] = "application/json"
        headers["Accept"] = "application/json"
        response = requests.get(url,headers=headers)
        if response.status_code == 200:
            return json.loads(response.text)
        else:
            em = "error connecting to runscope {}".format(response.status_code)
            handle_error(em)
    except Exception as e:
        handle_error("error while conneecting to runscope: " + str(e))

def get_test_bucket_details():
    test_bucket_details = []
    response = get_runscope_api_response(runscope_service_url)

    for data in response['data']:
        if data['name'] is not None and data['tests_url'] is not None and data['name']:
            if data['name'] == "CSP" or data['name'] == "service_health":
                test_bucket_meta_info = {}
                test_bucket_meta_info['bucket_name'] = data['name']
                test_bucket_meta_info['tests_url'] = data['tests_url']
                test_bucket_details.append(test_bucket_meta_info)

    return test_bucket_details

def get_test_details(test_bucket_list):
    test_details = []
    for bucket in test_bucket_list:

        response = get_runscope_api_response(bucket['tests_url'])
        for test in response['data']:
            test_meta_info = {}
            test_uuid = ""
            if test['last_run'] is not None:
               test_uuid = test['last_run']['test_uuid']

            if test_uuid is not None and len(test_uuid) > 0:
              results = str(get_runscope_api_response(bucket['tests_url'] + "/{}/results".format(test_uuid)))

              test_meta_info['latest_result_url'] = bucket['tests_url'] + "/{}/results/latest".format(test_uuid)
              test_meta_info['test_result_url'] = bucket['tests_url'] + "/{}/results".format(test_uuid)
              test_meta_info['test_id'] = test_uuid
              test_meta_info['test_name'] = test['name']
              test_meta_info['test_bucket_name'] = bucket['bucket_name']
              test_details.append(test_meta_info)
    return test_details

def build_metric_dict(dest_dict,dest_k,input_dict,first_key,second_key=None):
    val = input_dict.get(first_key,None)
    if val is not None and second_key:
        val = val.get(second_key,None)
    if val is not None:
        dest_dict[dest_k] = val


def get_test_latest_result(test_details):
    uber_result = []
    tests_finished_at_info = []

    since = (datetime.now() - timedelta(minutes = 5)).timestamp()
    does_finished_at_file_exist = os.path.exists(cache_file_test_finished_at)
    for test in test_details:
        response = []
        finished_at_details = []

        #if cache file doesn't exist use since, else use finished_at
        if does_finished_at_file_exist == False:
            response = get_runscope_api_response(test['test_result_url'] + "?count=50&since={}".format(since))
            results = response['data']
            if len(results) > 0:
                for result in results:
                    if result['finished_at'] is not None:
                        finished_at_details.append(result['finished_at'])

        else:
            finished_at_data = get_cached_test_info(cache_file_test_finished_at)
            for data in finished_at_data:
                if test['test_id'] == data['test_id']:
                    response = get_runscope_api_response(test['test_result_url'] + "?count=50&since={}".format(data['finished_at']))
                    results = response['data']
                    if len(results) > 0:
                        for result in results:
                            if result['finished_at'] is not None:
                                finished_at_details.append(result['finished_at'])

        #build response
        for resp in response['data']:
            service_metric = {}
            build_metric_dict(service_metric,'region',resp,'data','region')
            build_metric_dict(service_metric,'request_result', resp,'result')
            build_metric_dict(service_metric,'test_name',test,'test_name')
            build_metric_dict(service_metric,'test_bucket_name',test,'test_bucket_name')
            build_metric_dict(service_metric,'assertions_defined',resp,'assertions_defined')
            build_metric_dict(service_metric,'assertions_failed',resp,'assertions_failed')
            build_metric_dict(service_metric,'assertions_passed',resp,'assertions_passed')
            build_metric_dict(service_metric,'scripts_defined',resp,'scripts_defined')
            build_metric_dict(service_metric,'scripts_failed',resp,'scripts_failed')
            build_metric_dict(service_metric,'scripts_passed',resp,'scripts_passed')
            build_metric_dict(service_metric,'variables_defined',resp,'variables_defined')
            build_metric_dict(service_metric,'variables_failed',resp,'variables_failed')
            build_metric_dict(service_metric,'variables_passed',resp,'variables_passed')
            uber_result.append(service_metric)

        #build finished_at_info
        if len(finished_at_details) > 0:
            finished_at = max(finished_at_details)
        else:
            finished_at = since

        finished_at_info = {}
        finished_at_info['test_id'] = test['test_id']
        finished_at_info['finished_at'] = finished_at
        tests_finished_at_info.append(finished_at_info)

    #cache finished_at_info
    cache_info(tests_finished_at_info, cache_file_test_finished_at)

    if len(uber_result) > 0:
        return uber_result
    else:
        print("none")
        return None

def doMain():
    result = {}
    if is_cache_stale():
        data_to_cache = get_test_details(get_test_bucket_details())
        cache_info(data_to_cache, cache_file)
        result = get_test_latest_result(data_to_cache)
    else:
        ctm = get_cached_test_info(cache_file)
        if ctm is None:
            ctm = get_test_details(get_test_bucket_details())
            cache_info(ctm, cache_file)
        result = get_test_latest_result(ctm)

    if result is None:
        handle_error("Empty metric result")
    else:
        str_ = json.dumps(result,
                     indent=4, sort_keys=True,
                     separators=(',', ': '), ensure_ascii=False)
        print(str_)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('token', type=str, nargs=1, help='Runscope token.')
	args = parser.parse_args()
	doMain()
